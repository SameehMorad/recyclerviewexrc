package com.example.administrator.todolistsec

import android.content.Context


/**
 * Created by Sameeh on 16/01/2019.
 */
object InjectorUtils {

    fun provideTodosViewModelFactory(context: Context) : TodoViewModelFactory {
        val todoRepository = TodoRepository.getTodoRepository(AppDatabase.getDatabase(context).todoDao())

        return TodoViewModelFactory(todoRepository)
    }
}