package com.example.administrator.todolistsec

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment

class MainActivity : AppCompatActivity(), FragmentChanger {

    val manager = supportFragmentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ChangeFragment(1)
    }

    override fun ChangeFragment(fragmentNumber: Int) {
        val transaction = manager.beginTransaction()

        var fragment = Fragment()

        when (fragmentNumber) {
            1 -> fragment = MainFragment()
            2 -> fragment = AddFragment()
        }

        transaction.replace(R.id.fragment_holder, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}
