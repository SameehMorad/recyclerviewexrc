package com.example.administrator.todolistsec

import java.util.concurrent.Executor
import java.util.concurrent.Executors

/**
 * Created by Sameeh on 16/01/2019.
 */
class TodoRepository private constructor(private val todoDao: TodoDao) {

    private val executor: Executor = Executors.newSingleThreadExecutor()

    fun addTodo(todo: Todo) {
        executor.execute({ todoDao.add(todo) })
    }

    fun getAllTodos() = todoDao.getAll()


    fun updateTodoStatus(todoId: Int, status :Int) {
        executor.execute({ todoDao.updateToChecked(todoId, status) })
    }

    fun updateTodoEditted(todo: Todo) {
        executor.execute({
            todoDao.updateTodoEditted(todo)
        })
    }

    fun deleteList(todoList: List<Int>) {
        executor.execute {todoDao.deleteList(todoList)}
    }

    companion object {
        private var INSTANCE: TodoRepository? = null

        fun getTodoRepository(todoDao: TodoDao) =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: TodoRepository(todoDao).also { INSTANCE = it }
                }
    }
}