package com.example.administrator.todolistsec

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

/**
 * Created by Sameeh on 03/02/2019.
 */
class SharedViewModel : ViewModel() {
    val selectedTodo = MutableLiveData<Todo>()
    var editMode : Boolean = false

    fun select(todoToEdit: Todo) {
        selectedTodo.value = todoToEdit
    }
}