package com.example.administrator.todolistsec

/**
 * Created by Sameeh on 16/01/2019.
 */
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import kotlinx.android.synthetic.main.start_fragment.*
import android.support.v7.widget.DividerItemDecoration
import android.view.*

class MainFragment : Fragment(), RecyclerFunctions {

    lateinit var todoViewModel: TodoViewModel
    var selectedTodos = ArrayList<Int>()
    private lateinit var model: SharedViewModel

    val TAG = "MainFragment"
    private lateinit var fragmentChanger: FragmentChanger

    override fun onAttach(context: Context?) {
        Log.d(TAG, "onAttach")
        if (context is FragmentChanger) {
            fragmentChanger = context
        }

        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "onCreate")
        super.onCreate(savedInstanceState)

        model = activity?.run {
            ViewModelProviders.of(this).get(SharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        val factory = InjectorUtils.provideTodosViewModelFactory(context!!)
        todoViewModel = ViewModelProviders.of(this, factory).get(TodoViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        checkSizeAndShowDelete()
        // You can use GridLayoutManager if you want multiple columns. Enter the number of columns as a parameter.
        rv_todoList.layoutManager = GridLayoutManager(context, 1)

        // Access the RecyclerView Adapter and load the data into it
        rv_todoList.adapter = ItemAdapter(emptyList<Todo>(), context!!, this)

        btn_add_todo.setOnClickListener {
            model.editMode = false
            selectedTodos.clear()
            fragmentChanger.ChangeFragment(2)
        }

        // The line between the views
        rv_todoList.addItemDecoration(
                DividerItemDecoration(activity!!, DividerItemDecoration.VERTICAL)
        )

        todoViewModel.getAllTodos().observe(viewLifecycleOwner, Observer { (rv_todoList.adapter as ItemAdapter).setList(it!!) })
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.my_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_delete -> {
            todoViewModel.deleteList(selectedTodos.toList())
            selectedTodos.clear()

            checkSizeAndShowDelete()
            true
        }
        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }

    private fun checkSizeAndShowDelete() {
        if (selectedTodos.isNotEmpty()) {
            setHasOptionsMenu(true)
        } else {
            setHasOptionsMenu(false)
        }
    }

    override fun addTodosToDelete(todoId: Int) {
        selectedTodos.add(todoId)
        checkSizeAndShowDelete()
    }

    override fun removeTodosToDelete(todoId: Int) {
        selectedTodos.remove(todoId)
        checkSizeAndShowDelete()
    }

    override fun updateTodoStatus(todoId: Int, status : Int) {
        todoViewModel.updateTodoStatus(todoId, status)
    }

    override fun isMarked(todoId: Int): Boolean {
        return selectedTodos.contains(todoId)
    }

    override fun goToEdit(todoToEdit : Todo) {
        model.editMode = true
        model.select(todoToEdit)
        selectedTodos.clear()
        fragmentChanger.ChangeFragment(2)
    }

    override fun getDeleteListSize() : Int {
        return selectedTodos.size
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.start_fragment, container, false)
    }
}