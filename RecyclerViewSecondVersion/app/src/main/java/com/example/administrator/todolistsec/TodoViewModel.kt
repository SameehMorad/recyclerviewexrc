package com.example.administrator.todolistsec

import android.arch.lifecycle.ViewModel
import java.util.concurrent.Executors

/**
 * Created by Sameeh on 16/01/2019.
 */
class TodoViewModel(private val todoRepository: TodoRepository) : ViewModel() {

    fun getAllTodos() = todoRepository.getAllTodos()

    fun addTodo(todo: Todo) {
        todoRepository.addTodo(todo)
    }

    fun updateTodoStatus(todoId: Int, status : Int) {
        todoRepository.updateTodoStatus(todoId, status)
    }

    fun updateTodoEditted(todo: Todo) {
        todoRepository.updateTodoEditted(todo)
    }

    fun deleteList(todoList: List<Int>) {
        todoRepository.deleteList(todoList)
    }
}