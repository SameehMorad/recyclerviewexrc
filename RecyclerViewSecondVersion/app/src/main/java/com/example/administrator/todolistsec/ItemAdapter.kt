package com.example.administrator.todolistsec

/**
 * Created by Sameeh on 16/01/2019.
 */
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.support.v4.widget.CompoundButtonCompat.setButtonTintList
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.todo_list_item.view.*

class ItemAdapter(var items: List<Todo>, val context: Context, val listener: RecyclerFunctions) : RecyclerView.Adapter<ViewHolder>() {

    fun setList(todos: List<Todo>) {
        items = todos
        notifyDataSetChanged()
    }

    var firstLongClick = false


    override fun onBindViewHolder(viewHolder: ViewHolder, pos: Int) {
        viewHolder.tvTodoTitle?.text = items.get(pos).title
        viewHolder.tvTodoDesc?.text = items.get(pos).description
        viewHolder.tvTodoTime?.text = items.get(pos).time
        viewHolder.btnCheck.isChecked = items.get(pos).done
        viewHolder.itemView.setBackgroundColor(Color.DKGRAY)

        viewHolder.itemView.setOnLongClickListener {
            if (!firstLongClick) {
                if (!listener.isMarked(items.get(pos).id)) {
                    viewHolder.itemView.setBackgroundColor(Color.GRAY)
                    listener.addTodosToDelete(items.get(pos).id)
                    firstLongClick = true
                }
            }
            true
        }

        viewHolder.itemView.setOnClickListener {
            if (firstLongClick) {
                if (listener.isMarked(items.get(pos).id)) {
                    viewHolder.itemView.setBackgroundColor(Color.DKGRAY)
                    listener.removeTodosToDelete(items.get(pos).id)
                } else {
                    viewHolder.itemView.setBackgroundColor(Color.GRAY)
                    listener.addTodosToDelete(items.get(pos).id)
                }

                if (listener.getDeleteListSize() == 0){
                    firstLongClick= false
                }
            }

            true
        }

        if (items.get(pos).done) {
            setButtonTintList(viewHolder.btnCheck, ColorStateList.valueOf(Color.GREEN))
        } else {
            setButtonTintList(viewHolder.btnCheck, ColorStateList.valueOf(Color.RED))
        }

        viewHolder.btnCheck.setOnClickListener {
            if (!items.get(pos).done) {
                setButtonTintList(viewHolder.btnCheck, ColorStateList.valueOf(Color.GREEN))
                listener.updateTodoStatus(items.get(pos).id, 1)
                viewHolder.btnCheck.isChecked = true

            } else {
                setButtonTintList(viewHolder.btnCheck, ColorStateList.valueOf(Color.RED))
                listener.updateTodoStatus(items.get(pos).id, 0)
                viewHolder.btnCheck.isChecked = false
            }
        }


       // val b: Bundle = Bundle()
        viewHolder.btnDEdit.setOnClickListener {
         //   b.putParcelable("editTodo", items.get(pos))
            listener.goToEdit(items.get(pos))
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.todo_list_item, p0, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val tvTodoTitle = view.tv_todo_title
    val tvTodoDesc = view.tv_todo_desc
    val tvTodoTime = view.tv_todo_time
    val btnDEdit = view.btn_edit_todo
    val btnCheck = view.btn_done_todo
}