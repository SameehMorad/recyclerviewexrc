package com.example.administrator.todolistsec

import android.os.Bundle

/**
 * Created by Sameeh on 16/01/2019.
 */

interface FragmentChanger {
    fun ChangeFragment(fragmentNumber: Int)
}