package com.example.administrator.todolistsec

/**
 * Created by Sameeh on 16/01/2019.
 */
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import kotlinx.android.synthetic.main.add_edit_fragment.*
import java.text.SimpleDateFormat
import java.util.*
import android.arch.lifecycle.Observer

class AddFragment : Fragment() {

    val TAG = "AddFragemnt"
    private lateinit var fragmentChanger: FragmentChanger
    private lateinit var model: SharedViewModel

    override fun onAttach(context: Context?) {
        Log.d(TAG, "onAttach")
        if (context is FragmentChanger) {
            fragmentChanger = context
        }

        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "onCreate")
        super.onCreate(savedInstanceState)

        model = activity?.run {
            ViewModelProviders.of(this).get(SharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }

    private fun toastAndChangeFragemnt(toastMessage: String) {
        Toast.makeText(activity, toastMessage, Toast.LENGTH_LONG).show()
        fragmentChanger.ChangeFragment(1)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val factory = InjectorUtils.provideTodosViewModelFactory(context!!)
        val viewModel = ViewModelProviders.of(this, factory)
                .get(TodoViewModel::class.java)

        var todoEdit : Todo
        if (model.editMode) {
            model.selectedTodo.observe(this, Observer<Todo> { todoToEdit ->
                et_todo_title.setText(todoToEdit?.title)
                et_todo_desc.setText(todoToEdit?.description)
                todoEdit= todoToEdit!!
            })
        }

        btn_add_submit.setOnClickListener {
            if (TextUtils.isEmpty(et_todo_desc.getText())) {
                Toast.makeText(activity, "Enter Description", Toast.LENGTH_LONG).show()
            } else {
                val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss a")
                val currentDate = sdf.format(Date())

                if (model.editMode) {
                    model.selectedTodo.observe(this, Observer<Todo> { todoToEdit ->

                        todoToEdit?.title = et_todo_title.text.toString()
                        todoToEdit?.description = et_todo_desc.text.toString()
                        todoToEdit?.time = currentDate + " Editted"

                        viewModel.updateTodoEditted(todoToEdit!!)
                        toastAndChangeFragemnt("Todo Editted")

                    })
                } else {
                    val todoItem = Todo(
                            0,
                            et_todo_title.text.toString(),
                            et_todo_desc.text.toString(),
                            currentDate,
                            false
                    )
                    viewModel.addTodo(todoItem)

                    toastAndChangeFragemnt("Todo Added")
                }
                et_todo_desc.hideKeyboard()
            }
        }

        btn_cancel_add.setOnClickListener {
            toastAndChangeFragemnt("Todo Canceled")
        }
    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.add_edit_fragment, container, false)
    }
}
