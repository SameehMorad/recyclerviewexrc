package com.example.administrator.todolistsec

import android.os.Bundle

/**
 * Created by Sameeh on 16/01/2019.
 */
interface RecyclerFunctions {
    fun updateTodoStatus(todoId :Int, status : Int)
    fun addTodosToDelete(todoId: Int)
    fun removeTodosToDelete(todoId: Int)
    fun isMarked(todoId : Int) : Boolean
    fun goToEdit(todoToEdit : Todo)
    fun getDeleteListSize() : Int
}