package com.example.administrator.todolistsec

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*

/**
 * Created by Sameeh on 15/01/2019.
 */
@Dao
interface TodoDao {
    @Query("SELECT * FROM todos")
    fun getAll(): LiveData<List<Todo>>

    @Insert
    fun add(todo: Todo)

    @Query("UPDATE todos SET done = :status WHERE id = :todoId")
    fun updateToChecked(todoId : Int, status :Int)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateTodoEditted(todo: Todo)

    @Query("DELETE FROM todos WHERE id IN (:todoList)")
    fun deleteList(todoList: List<Int>)
}